def add(a, b):
    return a + b


def inc(x):
    if type(x) not in [int, float]:
        raise TypeError('the value should be int or float')
    return x + 1
